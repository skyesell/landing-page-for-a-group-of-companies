import { ReactNode } from 'react'

import Header from '@components/../../ui/Header'
import Footer from '@components/../../ui/Footer'

interface MainLayoutProps {
  children?: ReactNode
}

export const MainLayout = ({ children }: MainLayoutProps) => {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  )
}
