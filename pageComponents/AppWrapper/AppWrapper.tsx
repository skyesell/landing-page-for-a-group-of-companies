import { createContext, useContext, ReactNode, useState, useMemo, Dispatch, SetStateAction } from 'react'

interface IAppContext {
  isMapLoaded: boolean
  setMapLoaded?: Dispatch<SetStateAction<boolean>>
}

const defaultState = {
  isMapLoaded: false,
  map: null,
}

const AppContext = createContext<IAppContext>(defaultState)

export const AppWrapper = (props: { children: ReactNode }) => {
  const [isMapLoaded, setMapLoaded] = useState(false)
  const value = useMemo(() => ({ isMapLoaded, setMapLoaded }), [isMapLoaded])
  return <AppContext.Provider value={value}>{props.children}</AppContext.Provider>
}

export const useAppContext = () => {
  return useContext(AppContext)
}
