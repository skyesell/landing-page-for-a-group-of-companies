import React, { useState } from 'react'
import dynamic from 'next/dynamic'

import Button from '@ui/Button'

import styles from './styles.module.scss'
import ContactForm from '@ui/ContactForm'
import COMPANIES from '@projectTypes/companies'
import { ContactItem } from '@ui/ContactItem'
import { BackgroundHighlight } from '@ui/BackgroundHighlight'

const Jumbotron = () => {
  const [isModalOpened, setModalOpened] = useState(false)
  const Modal = dynamic(() => import('@ui/Modal'), { ssr: false })

  return (
    <>
      <Modal opened={isModalOpened} onClose={() => setModalOpened(false)}>
        <div className={styles.modalContent}>
          <h2>Мы свяжемся с Вами</h2>
          <p>Оставьте свои контактные данные и наш специалист ответит Вам в кратчайшие сроки</p>
          <ContactForm company={COMPANIES.DEFAULT} />
        </div>
      </Modal>
      <div id={'jumbotron'} className={styles.wrapper}>
        <BackgroundHighlight color={'#E754FF'} size={176} className={styles.imgWrapper1} />
        <BackgroundHighlight color={'#00FFFF'} size={176} className={styles.imgWrapper2} />
        <div className={styles.mainContent}>
          <div className={styles.leftSide}>
            <div className={styles.text}>
              <h2>Помогаем вашему&nbsp;бизнесу прокачаться до максимального уровня</h2>
              <p>
                Уделяем особое внимание проектированию интерфейсов,
                <br />в&nbsp;результате чего мы с гордостью можем назвать наши продукты Надёжными!
              </p>
              <Button
                text={'старт с нами'}
                className={styles.button}
                onClick={() => setModalOpened(true)}
                type={'button'}
              />
            </div>
            <aside>
              <div className={styles.line} />
              <p>
                Наша команда остаётся на связи в любое время!
                <br />
                Будем рады ответить на Ваши вопросы
              </p>
            </aside>
          </div>
          <div className={styles.imgPhoneWrapper}>
            <img loading={'eager'} alt={'Технологии надёжности'} src={'/assets/phone.png'} />
          </div>
        </div>
        <div className={styles.information}>
          <div className={styles.contacts}>
            <ContactItem label={'Приёмная'} iconPath={'/assets/tel.svg'} data={'+7 (800) 555-30-53'} kind={'phone'} />
            <ContactItem label={'Менеджер'} iconPath={'/assets/email.svg'} data={'tn@reliab.tech'} kind={'email'} />
          </div>
          <div className={styles.business}>
            <div className={styles.item}>{`Разработка\nПО`}</div>
            <div className={styles.item}>{`Аудит\nи автоматизация бизнеса`}</div>
            <div className={styles.item}>{`Разработка\nэлектроники`}</div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Jumbotron
