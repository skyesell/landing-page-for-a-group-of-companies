import React from 'react'
import cs from 'classnames'

import s from './styles.module.scss'
import Image from 'next/image'
import bigBossPhoto from '@assets/mainMan.webp'
import { Breadcrumbs } from '@ui/Breadcrumbs'
import { BackgroundHighlight } from '@ui/BackgroundHighlight'

const JurisdictionInfo = ({ mob }: { mob?: boolean }) => {
  const jurisdictionClassName = cs(s.jurisdiction, { [s.mob]: mob })

  return (
    <div className={jurisdictionClassName}>
      <h2>Мы - специалисты в разных областях IT</h2>
      <div className={s.items}>
        <div className={s.item}>
          <h3>Разработка ПО</h3>
          <p>
            Разработка и поддержка программных продуктов для web, desktop, iOS, Android, UI/UX-дизайн, IT-аутсорсинг и
            аутстафинг
          </p>
        </div>
        <div className={s.item}>
          <h3>Аудит и автоматизация бизнеса</h3>
          <p>
            IT-консалтинг, оптимизация бизнес-процессов, лицензирование программных продуктов 1С, конфигурирование
            средствами BPMS
          </p>
        </div>
        <div className={s.item}>
          <h3>Разработка электроники</h3>
          <p>Беспилотные технологии, георадиолокация, радарные системы, подповерхностное зондирование</p>
        </div>
        <div className={s.item}>
          <h3>Продвижение, оптимизация, маркетинг</h3>
          <p>SEO, контент-менеджмент, копирайтинг, линкбилдинг, модерация и аналитика</p>
        </div>
      </div>
    </div>
  )
}

export const MainInfo = () => {
  return (
    <div className={s.wrapper}>
      <BackgroundHighlight color={'#E754FF'} size={200} className={s.hl1} />
      <BackgroundHighlight color={'#00FFFF'} size={176} className={s.hl2} />
      <Breadcrumbs />
      <div className={s.mainInfo}>
        <div className={s.info}>
          <h1>НПО Технологии Надёжности</h1>
          <div className={s.infoText}>
            <p>Группа компаний «Технологии Надежности» работает на IT-рынке с 2009 года.</p>
            <p>
              Технические, технологические и кадровые ресурсы позволяют эффективно сотрудничать с компаниями в форматах
              отдельных проектов и выделенных центров компетенции.
              <br />
              <br />
              Крупные промышленные предприятия, банки и страховые компании стали постоянными клиентами, доверив
              специалистам «Технологии Надежности» разработку и поддержку программного обеспечения, электроники и других
              IT-проектов.
            </p>
          </div>
          <JurisdictionInfo />
        </div>
        <div className={s.ceo}>
          <div className={s.imageContainer}>
            <Image src={bigBossPhoto} alt={'CEO'} layout={'intrinsic'} priority />
          </div>
          <h3>Дубовицкий Александр Николаевич</h3>
          <p>
            Генеральный директор и основатель <br /> Группы компаний «Технологии Надёжности»
          </p>
        </div>
        <JurisdictionInfo mob />
      </div>
    </div>
  )
}
