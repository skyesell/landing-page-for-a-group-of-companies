import cs from 'classnames'

import s from './styles.module.scss'
import { forwardRef, useEffect, useRef } from 'react'

interface VacancyItemProps {
  vacancyName: string
  publicationDate: string
  companyName: string
  vacancyDescription: string
  companyEmail: string
  telegramLink?: string
  isOpen: boolean
  onOpen: () => void
}

// eslint-disable-next-line react/display-name
export const VacancyItem = forwardRef<HTMLDivElement, VacancyItemProps>(
  (
    {
      vacancyDescription,
      vacancyName,
      companyName,
      companyEmail,
      isOpen = true,
      publicationDate,
      telegramLink,
      onOpen,
    },
    ref
  ) => {
    const descriptionRef = useRef<HTMLDivElement>(null)

    useEffect(() => {
      const descriptionNode = descriptionRef.current
      if (descriptionNode) {
        if (isOpen) descriptionNode.style.height = `${descriptionNode.scrollHeight}px`
        else descriptionNode.style.height = '0'
      }
    }, [isOpen])

    return (
      <div className={s.vacancyItem} ref={ref}>
        <div className={s.header} onClick={onOpen}>
          <div>
            <div className={s.publicationDate}>{publicationDate}</div>
            <div className={cs(s.title, { [s.active]: isOpen })}>
              <h3>{vacancyName}</h3>
              <div className={s.companyName}>{`«${companyName}»`}</div>
            </div>
          </div>
          <button className={cs(s.openIcon, { [s.opened]: isOpen })} onClick={onOpen}>
            <div className={s.horizontalLine} />
            <div className={s.verticalLine} />
          </button>
        </div>
        <div className={s.content} ref={descriptionRef}>
          <div
            className={cs(s.description, { [s.opened]: isOpen })}
            dangerouslySetInnerHTML={{ __html: vacancyDescription }}
          />
          <div className={s.contacts}>
            <div className={s.email}>
              Отправить резюме на <a href={`mailto:${companyEmail}`}>{companyEmail}</a>
            </div>
            <>
              <div className={s.or}>или</div>
              <div className={s.telegram}>
                написать нам в <a>Telegram</a>
              </div>
            </>
          </div>
          <div className={s.divider} />
        </div>
      </div>
    )
  }
)
