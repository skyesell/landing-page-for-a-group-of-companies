import React from 'react'
import cn from 'classnames'
import Link from 'next/link'
import { useRouter } from 'next/router'

import BtnCloseMenu from '@public/assets/btnCloseMenu.svg'
import LogoMobile from '@public/assets/logoMobile.svg'

import styles from './styles.module.scss'
import { routeConfig } from '../../routeConfig'

interface MobileMenuProps {
  onClose: () => void
}

const MobileMenu = ({ onClose }: MobileMenuProps) => {
  const [closeAnimation, setCloseAnimation] = React.useState<boolean>(false)
  const router = useRouter()

  React.useEffect(() => {
    document.body.style.position = 'fixed'
    document.body.style.overflow = 'hidden'
    return () => {
      document.body.style.overflow = 'auto'
      document.body.style.position = 'relative'
      onClose()
    }
  }, [onClose])

  const handleClose = () => {
    setCloseAnimation(true)
    const timeout = setTimeout(() => {
      onClose()
    }, 500)

    return () => {
      clearTimeout(timeout)
    }
  }

  const wrapperStyles = cn(styles.wrapper, {
    [styles.closing]: closeAnimation,
  })

  const getRouteClassName = (path: string) =>
    cn(styles.item, { [styles.current]: router.pathname.split('/')[1] === path })

  return (
    <div className={wrapperStyles}>
      <div className={styles.head}>
        <LogoMobile />
        <div className={styles.btnMenu} onClick={handleClose}>
          <BtnCloseMenu />
        </div>
      </div>
      <div className={styles.mainContent}>
        {routeConfig.mainRoutes.map((route) => (
          <div className={getRouteClassName(route.path)} key={route.path}>
            <Link href={`/${route.path}`}>{route.name}</Link>
          </div>
        ))}
      </div>
      <div className={styles.contacts}>
        <div className={styles.item}>
          <h3>Приёмная</h3>
          <a href={'tel:+7 (800) 555-30-53'}>+7 (800) 555-30-53</a>
        </div>
        <div className={styles.item}>
          <h3>Менеджер</h3>
          <a href={'mailto:tn@reliab.tech'}>tn@reliab.tech</a>
        </div>
      </div>
    </div>
  )
}

export default MobileMenu
