import cn from 'classnames'

import styles from './styles.module.scss'

interface TextAreaProps {
  placeholder: string
  value: string
  onChange: (value: string) => void
  error?: boolean
  helperText?: string
  inputColor?: string
}

const TextArea = ({ placeholder, value, onChange, error, inputColor }: TextAreaProps) => {
  const inputClassName = cn(styles.textArea, {
    [styles.error]: error,
  })

  return (
    <textarea
      style={{ background: inputColor }}
      className={inputClassName}
      placeholder={placeholder}
      value={value}
      onChange={(e) => onChange(e.target.value)}
    />
  )
}

export default TextArea
