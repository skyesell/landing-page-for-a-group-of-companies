import Script from 'next/script'
import { useEffect } from 'react'
import s from './styles.module.scss'
import { useAppContext } from '@pageComponents/AppWrapper/AppWrapper'
const DG = require('2gis-maps')

const Map = () => {
  const { isMapLoaded, setMapLoaded } = useAppContext()

  const showMap = () => {
    DG.then(() => {
      const map = DG.map('mapRoot', {
        center: { lon: 36.571247, lat: 50.571265 },
        fullscreenControl: false,
        zoom: 20,
      })

      DG.marker({ lon: 36.571247, lat: 50.571265 }, { title: 'Мы здесь' }).addTo(map)
    })
  }

  const onLoad = () => {
    if (window !== undefined && setMapLoaded) {
      setMapLoaded(true)
      showMap()
    }
  }
  useEffect(() => {
    if (isMapLoaded) showMap()
  }, [])

  return (
    <div className={s.maps}>
      <div id="mapRoot" className={s.mapsRoot}>
        {!isMapLoaded && <div className={s.fallback}>Загрузка...</div>}
      </div>
      <Script
        crossOrigin="anonymous"
        src="https://maps.api.2gis.ru/2.0/loader.js"
        strategy="lazyOnload"
        onLoad={onLoad}
      />
    </div>
  )
}

export default Map
