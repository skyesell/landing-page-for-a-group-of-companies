import React from 'react'
import ContactForm from '@ui/ContactForm'
import styles from './styles.module.scss'
import { BackgroundHighlight } from '@ui/BackgroundHighlight'

interface ContactsProps {
  mapsImgUrl?: string
  gisImgUrl?: string
  disableHighlight?: boolean
  buttonColor?: string
  inputColor?: string
  headerColor?: string
  textColor?: string
}

const Contacts = (props: ContactsProps) => {
  const {
    mapsImgUrl = '/assets/map1.svg',
    gisImgUrl = '/assets/map2.svg',
    disableHighlight,
    buttonColor = '#ffffff',
    inputColor = '#E9EFF6',
    headerColor = '#000000',
    textColor = '#8a96af',
  } = props
  return (
    <div id={'contacts'} className={styles.wrapper}>
      {!disableHighlight && (
        <>
          <BackgroundHighlight color={'#00FFFF'} size={120} className={styles.imgWrapper1} />
          <BackgroundHighlight color={'#E754FF'} size={120} className={styles.imgWrapper2} />
        </>
      )}
      <div className={styles.content}>
        <div className={styles.text}>
          <h2 style={{ color: headerColor }}>
            Хотите получить <br /> самый быстрый ответ?
          </h2>
          <p style={{ color: textColor }}>
            Мы проверяем почту каждый час и с радостью ответим на любой Ваш вопрос. Наш менеджер свяжется со
            специалистами, чтобы вы получили самый компетентный ответ
          </p>
          <div className={styles.maps}>
            <a
              style={{ background: buttonColor }}
              href={'https://goo.gl/maps/VUyGjbXkc4zgT8c46'}
              rel="noreferrer"
              target="_blank"
            >
              <img src={mapsImgUrl} alt={'Google Maps'} width={110} height={23} />
            </a>
            <a style={{ background: buttonColor }} href={'https://go.2gis.com/5z7ask'} rel="noreferrer" target="_blank">
              <img src={gisImgUrl} alt={'2GIS'} width={53} height={22} />
            </a>
          </div>
        </div>
        <div className={styles.contacts}>
          <div className={styles.maps}>
            <a
              style={{ background: buttonColor }}
              href={'https://goo.gl/maps/VUyGjbXkc4zgT8c46'}
              rel="noreferrer"
              target="_blank"
            >
              <img src={mapsImgUrl} alt={'Google Maps'} width={184} height={38} />
            </a>
            <a style={{ background: buttonColor }} href={'https://go.2gis.com/5z7ask'} rel="noreferrer" target="_blank">
              <img src={gisImgUrl} alt={'2GIS'} width={89} height={38} />
            </a>
          </div>
          <div className={styles.form}>
            <ContactForm inputColor={inputColor} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contacts
