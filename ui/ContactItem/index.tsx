import s from './styles.module.scss'
import Image from 'next/image'

interface ContactItemProps {
  label: string
  iconPath: string
  data: string
  kind: 'phone' | 'email' | 'text'
}

export const ContactItem = ({ data, kind, label, iconPath }: ContactItemProps) => {
  const getHref = () => {
    if (kind === 'phone') return `tel:${data}`
    if (kind === 'email') return `mailto:${data}`
    return ''
  }

  return (
    <div className={s.content}>
      <div>
        <Image src={iconPath} height={50} width={50} alt={''} />
      </div>
      <div className={s.info}>
        <h3>{label}</h3>
        {kind === 'text' ? (
          <div className={s.data}>{data}</div>
        ) : (
          <a className={s.data} href={getHref()}>
            {data}
          </a>
        )}
      </div>
    </div>
  )
}
