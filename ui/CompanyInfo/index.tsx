import Image from 'next/image'

import s from './styles.module.scss'
import { ReactNode } from 'react'
import { ContactItem } from '@ui/ContactItem'
import { Breadcrumbs } from '@ui/Breadcrumbs'

interface CompanyInfoProps {
  name: string
  websiteLink: string
  address: string
  phone: string
  email: string
  children?: ReactNode
}

export const CompanyInfo = ({ address, phone, websiteLink, name, email, children }: CompanyInfoProps) => {
  return (
    <div className={s.wrapper}>
      <Breadcrumbs />
      <div className={s.companyInfo}>
        <div className={s.title}>
          <h1>{name}</h1>
          <a href={websiteLink}>
            <Image src={'/assets/projectLink.svg'} height={27} width={27} alt={''} />
          </a>
        </div>
        <div className={s.content}>{children}</div>
        <div className={s.contacts}>
          <ContactItem label={'Адрес'} iconPath={'/assets/tel.svg'} data={address} kind={'text'} />
          <ContactItem label={'Приёмная'} iconPath={'/assets/tel.svg'} data={phone} kind={'phone'} />
          <ContactItem label={'Менеджер'} iconPath={'/assets/email.svg'} data={email} kind={'email'} />
        </div>
      </div>
    </div>
  )
}
