import React, { useState } from 'react'
import cs from 'classnames'
import dynamic from 'next/dynamic'

import ButtonOpenMenu from '@public/assets/btnOpenMenu.svg'
import ImgLogo from '@public/assets/logo.svg'
import ImgLogoLight from '@assets/logoLight.svg'

import styles from './styles.module.scss'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { routeConfig } from '../../routeConfig'

interface HeaderProps {
  theme?: 'dark' | 'light'
  CustomMenuButton?: any
}

const Header = ({ theme = 'dark', CustomMenuButton = ButtonOpenMenu }: HeaderProps) => {
  const [openedMenu, setOpenedMenu] = useState<boolean>(false)
  const router = useRouter()

  const getNavbarClassName = cs(styles.navbar, { [styles.light]: theme === 'light' })
  const getRouteClassName = (path: string) =>
    cs(styles.item, { [styles.current]: router.pathname.split('/')[1] === path })

  const MobileMenu = dynamic(() => import('@ui/MobileMenu'), { ssr: false })

  return (
    <>
      {openedMenu && <MobileMenu onClose={() => setOpenedMenu(false)} />}
      <div className={styles.wrapper}>
        <header>
          <Link href={'/'} passHref>
            <a className={styles.imgLogoWrapper}>
              {theme === 'dark' && <ImgLogo />}
              {theme === 'light' && <ImgLogoLight />}
            </a>
          </Link>
          <div className={getNavbarClassName}>
            {routeConfig.mainRoutes.map((route, index) => (
              <React.Fragment key={route.path}>
                {index !== 0 && <div className={styles.ellipse} />}
                <div className={getRouteClassName(`${route.path}`)}>
                  <Link href={`/${route.path}`}>{route.name}</Link>
                </div>
              </React.Fragment>
            ))}
          </div>
          <div className={styles.btnMenu} onClick={() => setOpenedMenu(true)}>
            <CustomMenuButton />
          </div>
        </header>
      </div>
    </>
  )
}

export default Header
