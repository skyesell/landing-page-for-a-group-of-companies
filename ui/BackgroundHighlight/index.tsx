import cs from 'classnames'

import s from './styles.module.scss'

interface BackgroundHighlightProps {
  color: string
  size: number
  className?: string
}

export const BackgroundHighlight = ({ color, size, className }: BackgroundHighlightProps) => {
  return (
    <div
      className={cs(s.highlight, className)}
      style={{
        boxShadow: `0px 0px 100px ${size}px ${color}`,
        background: color,
      }}
    />
  )
}
