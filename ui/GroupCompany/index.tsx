import React from 'react'
import Company from './components/Company'
import COMPANIES from '@projectTypes/companies'
import styles from './styles.module.scss'
import { BackgroundHighlight } from '@ui/BackgroundHighlight'

const GroupCompany = () => {
  return (
    <>
      <div id={'aboutUs'} className={styles.wrapper}>
        <BackgroundHighlight color={'#9EACF1'} size={165} className={styles.imgWrapper1} />
        <BackgroundHighlight color={'#9753DD'} size={165} className={styles.imgWrapper2} />
        <div className={styles.text}>
          <span className={styles.text1}>Технологии надёжности</span>
          <span className={styles.text2}>
            <span>- это объединение </span>
            <span>IT-специалистов</span>
            <span> из самых разных областей информационного пространства</span>
          </span>
        </div>
        <div className={styles.information}>
          <Company
            image={'/assets/pirs.svg'}
            header={COMPANIES.PIRS}
            text={'Разработка ПО под заказ'}
            casesRoute={'/about/pirs'}
          />
          <div className={styles.verticalLine} />
          <div className={styles.lineColumn} />
          <Company
            image={'/assets/geoexpert.svg'}
            header={COMPANIES.GEOEXPERT}
            text={'Георадиолокация и радарные системы для подповерхностного зондирования и навигации'}
            casesRoute={'/about/geoexpert'}
          />
          <div className={styles.horizontalLine} />
          <div className={styles.emptyBlock} />
          <div className={styles.horizontalLine} />
          <div className={styles.lineColumn} />
          <Company
            image={'/assets/kvanta.svg'}
            header={COMPANIES.KVANTA}
            text={'Разработка электроники'}
            casesRoute={'/about/kvanta'}
          />
          <div className={styles.verticalLine} />
          <div className={styles.lineColumn} />
          <Company
            image={'/assets/element.svg'}
            header={COMPANIES.FIVE_ELEMENT}
            text={'Аутстаффинг IT-специалистов'}
            casesRoute={'/about/five-element'}
          />
          <div className={styles.horizontalLine} />
          <div className={styles.emptyBlock} />
          <div className={styles.horizontalLine} />
          <div className={styles.lineColumn} />
          <Company
            image={'/assets/expertChoice.svg'}
            header={COMPANIES.EXPERT_CHOICE_CIS}
            text={'Аудит и автоматизация бизнеса'}
            casesRoute={'/about/expert-choice-cis'}
          />
          <div className={styles.verticalLine} />
          <div className={styles.lineColumn} />
          <Company
            image={'/assets/katespark.svg'}
            header={COMPANIES.KATESPARK}
            text={'Рекрутинговое агентство'}
            casesRoute={'/about/kate-spark'}
          />
        </div>
      </div>
    </>
  )
}

export default GroupCompany
