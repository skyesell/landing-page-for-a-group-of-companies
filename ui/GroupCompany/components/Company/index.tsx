import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

import COMPANIES from '@projectTypes/companies'
import styles from './styles.module.scss'

interface CompanyProps {
  image: string
  header: COMPANIES
  casesRoute: string
  text: string
}

const Company = (props: CompanyProps) => {
  const { image, header, text, casesRoute } = props

  return (
    <>
      <article className={styles.company}>
        <div className={styles.header}>
          <Image src={image} alt={header} width={77} height={77} />
          <h2>{header}</h2>
        </div>
        <div className={styles.companyInfo}>
          <h2>{header}</h2>
          <p>{text}</p>
          <div className={styles.additionalInfo}>
            <Link href={casesRoute}>ПОДРОБНЕЕ</Link>
          </div>
        </div>
      </article>
    </>
  )
}

export default Company
