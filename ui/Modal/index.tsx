import React from 'react'
import ReactDOM from 'react-dom'

import BtnCloseModal from '@public/assets/closeModalIcon.svg'

import styles from './styles.module.scss'

interface ModalProps {
  children: React.ReactNode
  onClose: () => void
  opened: boolean
}

const Modal = ({ children, onClose, opened }: ModalProps) => {
  const handleKeyDown = React.useCallback(
    (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        onClose()
      }
    },
    [onClose]
  )

  React.useEffect(() => {
    if (opened) {
      document.body.style.overflow = 'hidden'
      document.addEventListener('keydown', handleKeyDown)
    }
    return () => {
      document.body.style.paddingRight = '0'
      document.body.style.overflow = 'auto'
      document.removeEventListener('keydown', handleKeyDown)
    }
  }, [opened, handleKeyDown])

  if (!opened) {
    return null
  }

  return ReactDOM.createPortal(
    <div className={styles.modalWrapper}>
      <div onClick={onClose} className={styles.background} />
      <div className={styles.modal}>
        <div className={styles.closeModalIcon} onClick={onClose}>
          <BtnCloseModal />
        </div>
        {children}
      </div>
    </div>,
    document.body
  )
}

export default Modal
