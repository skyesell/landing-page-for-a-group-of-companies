import React, { useEffect, useState } from 'react'
import { NextRouter, useRouter } from 'next/router'
import cs from 'classnames'
import s from './styles.module.scss'
import Link from 'next/link'
import { routeConfig } from '../../routeConfig'

type Breadcrumb = { routeName: string; path: string }

interface BreadcrumbsProps {
  currentPageName?: string
  modifyBreadcrumbStyle?: string
  modifyBreadcrumbsStyle?: string
  modifyLastBreadcrumbStyle?: string
  modifyDividerStyle?: string
}

//TODO рефактор?
const getBreadcrumbs = (router: NextRouter, currentPageName: string): Breadcrumb[] => {
  const currentPaths = router.pathname.split('/')
  return currentPaths.reduce((acc, curr, index) => {
    const elem = routeConfig.mainRoutes.concat(routeConfig.breadcrumbsOnly).find((route) => route.path === curr)
    if (elem) {
      if (index === 0) return [...acc, { routeName: elem.name, path: '/' }]
      if (index === 1) return [...acc, { routeName: elem.name, path: `/${elem.path}` }]
      return [...acc, { routeName: elem.name, path: `${acc[index - 1].path}/${curr}` }]
    }
    return [...acc, { routeName: currentPageName, path: router.pathname }]
  }, [] as Breadcrumb[])
}

export const Breadcrumbs = ({
  currentPageName = 'Set name!',
  modifyBreadcrumbStyle,
  modifyDividerStyle,
  modifyLastBreadcrumbStyle = '',
  modifyBreadcrumbsStyle,
}: BreadcrumbsProps) => {
  const [breadcrumbs, setBreadcrumbs] = useState<Breadcrumb[]>([])
  const router = useRouter()

  useEffect(() => {
    setBreadcrumbs(getBreadcrumbs(router, currentPageName))
  }, [currentPageName, router])

  const dividerStyle = cs(s.divider, modifyDividerStyle)

  return (
    <div className={cs(s.breadcrumbWrapper, modifyBreadcrumbsStyle)}>
      {breadcrumbs.map((breadcrumb, index) => (
        <React.Fragment key={breadcrumb.path}>
          <div
            className={cs(s.breadcrumb, modifyBreadcrumbStyle, {
              [s.last]: index === breadcrumbs.length - 1 && !modifyLastBreadcrumbStyle,
              [modifyLastBreadcrumbStyle]: index === breadcrumbs.length - 1,
            })}
          >
            {breadcrumbs.length - 1 === index ? (
              <div>{breadcrumb.routeName}</div>
            ) : (
              <Link href={breadcrumb.path}>{breadcrumb.routeName}</Link>
            )}
          </div>
          {index !== breadcrumbs.length - 1 && <div className={dividerStyle} />}
        </React.Fragment>
      ))}
    </div>
  )
}
