import { SVGProps } from "react";
import styles from './styles.module.scss'

const SuccessIcon = (props: SVGProps<SVGSVGElement>) => (
    <div className={styles.toast}>
        <svg width={14} height={10} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
            <path
                d="M12.719 0c-.3.01-.584.134-.793.349-.26.26-1.981 2.03-3.759 3.855-1.443 1.482-2.46 2.525-3.032 3.11L1.91 4.86A1.153 1.153 0 1 0 .517 6.69l4.035 3.075a1.153 1.153 0 0 0 1.513-.102c.26-.26 1.982-2.024 3.76-3.85 1.777-1.825 3.61-3.713 3.735-3.837A1.154 1.154 0 0 0 12.719 0Z"
                fill="#91C95A"
            />
        </svg>
        <div>{props.children}</div>
    </div>

)

export default SuccessIcon
