import React from 'react'

export const useRequestAnimation = (callback: () => void) => {
  const requestRef = React.useRef(0)

  React.useEffect(() => {
    const animate = () => {
      callback()
      requestRef.current = requestAnimationFrame(animate)
    }
    requestRef.current = requestAnimationFrame(animate)
    return () => cancelAnimationFrame(requestRef.current)
  }, [])
}
