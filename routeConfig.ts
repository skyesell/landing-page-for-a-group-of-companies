export const routeConfig = {
  mainRoutes: [
    { path: '', name: 'Главная' },
    { path: 'about', name: 'О нас' },
    { path: 'blog', name: 'Блог' },
    { path: 'contacts', name: 'Контакты' },
    { path: 'vacancy', name: 'Вакансии' },
  ],
  breadcrumbsOnly: [
    { path: 'pirs', name: 'Пирс' },
    { path: 'kvanta', name: 'KVANTA' },
    { path: 'five-element', name: 'Пятый элемент' },
    { path: 'expert-choice-cis', name: 'Expert Choice CIS' },
    { path: 'geoexpert', name: 'Geoexpert' },
    { path: 'pirs', name: 'Пирс' },
    { path: 'kate-spark', name: 'Kate Spark' },
  ],
}
