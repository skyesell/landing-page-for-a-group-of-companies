import Image from 'next/image'
import Link from 'next/link'

import s from './styles.module.scss'
import { newsApi } from '@api/news'
import { BlogT } from '@api/news/types'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { Breadcrumbs } from '@ui/Breadcrumbs'
import Contacts from '@ui/Contacts'

const SingleBlog = ({ blogData }: { blogData: BlogT }) => {
  const { title, publicationDate, description } = blogData
  return (
    <MetaLayout pageTitle={title}>
      <MainLayout>
        <div className={s.singleBlog}>
          <Breadcrumbs currentPageName={title} modifyBreadcrumbsStyle={s.breadcrumbs} />
          <div className={s.title}>
            <div className={s.backArrow}>
              <Link href={'/blog'}>
                <a>
                  <Image src={'/assets/backArrow.svg'} width={30} height={30} alt={'back arrow'} />
                </a>
              </Link>
            </div>
            <h1>{title}</h1>
          </div>
          <div className={s.date}>{publicationDate}</div>
          <div className={s.description} dangerouslySetInnerHTML={{ __html: description }} />
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export const getServerSideProps = async (context: any) => {
  const blogData = await newsApi.getSingleBlog(context.query.id)
  return { props: { blogData } }
}

export default SingleBlog
