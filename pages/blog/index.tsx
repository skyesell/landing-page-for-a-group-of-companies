import Link from 'next/link'
import React, { useState } from 'react'

import s from './styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { newsApi, BlogT } from '@api/news'
import { Breadcrumbs } from '@ui/Breadcrumbs'
import Contacts from '@ui/Contacts'
import { baseUrl, PaginationT } from '@api'
import { BackgroundHighlight } from '@ui/BackgroundHighlight'

const Blog = ({ attributes, pagination }: { attributes: BlogT[]; pagination: PaginationT }) => {
  const [news, setNews] = useState(attributes)
  const [page, setPage] = useState(1)

  const showMore = async () => {
    const res = await newsApi.getMultipleBlogs(page + 1)
    setPage(res.pagination.page)
    setNews(news.concat(res.attributes))
  }

  return (
    <MetaLayout pageTitle={'Блог'}>
      <MainLayout>
        <div className={s.blog}>
          <BackgroundHighlight color={'#E754FF'} size={200} className={s.hl1} />
          <BackgroundHighlight color={'#00FFFF'} size={176} className={s.hl2} />
          <Breadcrumbs />
          <h1>Следите за новостями</h1>
          <div className={s.news}>
            {news.map((n) => (
              <Link href={`/blog/${n.friendlyURL}`} key={n.id}>
                <a className={s.item}>
                  <div
                    className={s.thumbnailContainer}
                    style={{
                      background: `url(${baseUrl}${n.thumbnail!.data.attributes.url})
                       no-repeat center/cover`,
                    }}
                  />
                  <div className={s.content}>
                    <h2>{n.title}</h2>
                    <div className={s.date}>{n.publicationDate}</div>
                    <div className={s.description}>{n.previewDescription}</div>
                  </div>
                </a>
              </Link>
            ))}
          </div>
          {page < pagination.pageCount && (
            <div className={s.showMoreWrapper}>
              <div />
              <button className={s.showMore} onClick={showMore}>
                Показать еще
              </button>
            </div>
          )}
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export const getServerSideProps = async () => {
  const newsFromRes = await newsApi.getMultipleBlogs()
  return { props: { attributes: newsFromRes.attributes, pagination: newsFromRes.pagination } }
}

export default Blog
