import type { AppProps } from 'next/app'
import React from "react";

import 'scroll-behavior-polyfill'

import '@styles/globals.scss'
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css";
import { AppWrapper } from '@pageComponents/AppWrapper/AppWrapper'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AppWrapper>
      <Component {...pageProps} />
      <ToastContainer/>
    </AppWrapper>
  )
}

export default MyApp
