import s from '../styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { CompanyInfo } from '@ui/CompanyInfo'
import Contacts from '@ui/Contacts'
import COMPANIES from '@projectTypes/companies'

const KateSpark = () => {
  return (
    <MetaLayout pageTitle={COMPANIES.KATESPARK}>
      <MainLayout>
        <div className={s.aboutCompany}>
          <CompanyInfo
            name={COMPANIES.KATESPARK}
            websiteLink={'https://www.google.ru/'}
            address={'308034, Россия, г. Белгород, ул. Королёва, д. 2а, корпус 2, офис 512'}
            phone={'+7 (800) 555-30-53'}
            email={'tn@reliab.tech'}
          >
            <div className={s.text}>
              <p>
                Компания «ПИРС» является разработчиком программных и технических систем и комплексов для бизнеса в
                сферах: производства, ритейла, медицины, финансов, транспорта и логистики.
              </p>
              <br />
              <p>
                Компания занимается разработкой приложений любой сложности с учетом потребностей и особенностей вашего
                бизнеса, оказывает услуги по разработке, модернизации, тестированию и сопровождению ПО в форматах
                аутсорсинга, аутстаффинга, контрактной разработки и отдельных проектов.
              </p>
              <br />
              <p>
                Специализируется на разработке программного обеспечения и решении задач по автоматизации
                бизнес-процессов, в том числе — уникальных и нестандартных. Специалистами компании реализовано более 100
                проектов — корпоративные системы, программные решения (в том числе — приложения для мобильных
                устройств), чат-боты, системы технического зрения.
              </p>
            </div>
          </CompanyInfo>
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export default KateSpark
