import s from '../styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { CompanyInfo } from '@ui/CompanyInfo'
import Contacts from '@ui/Contacts'
import COMPANIES from '@projectTypes/companies'

const Geoexpert = () => {
  return (
    <MetaLayout pageTitle={COMPANIES.GEOEXPERT}>
      <MainLayout>
        <div className={s.aboutCompany}>
          <CompanyInfo
            name={COMPANIES.GEOEXPERT}
            websiteLink={'https://www.google.ru/'}
            address={'308033, Россия, Белгород, ул. Королёва, 2а, корпус 2'}
            phone={'+7 (951) 134-68-00'}
            email={'geoexpert@reliab.tech'}
          >
            <div className={s.text}>
              <p>
                Компания "Геоэксперт" разрабатывает и внедряет георадиолокационные системы и оказывает услуги по
                георадарному зондированию.
              </p>
              <br />
              <p>
                Используется оборудование собственной разработки на основе технологии Stepped Frequency Continuous Wave
                (SFCW). Предоставляются услуги по обследованию состояния гидротехнических, промышленных сооружений и
                объектов городской инфраструктуры.
              </p>
            </div>
          </CompanyInfo>
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export default Geoexpert
