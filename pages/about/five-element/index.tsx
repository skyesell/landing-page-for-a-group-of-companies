import s from '../styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { CompanyInfo } from '@ui/CompanyInfo'
import Contacts from '@ui/Contacts'
import COMPANIES from '@projectTypes/companies'

const FiveElement = () => {
  return (
    <MetaLayout pageTitle={COMPANIES.FIVE_ELEMENT}>
      <MainLayout>
        <div className={s.aboutCompany}>
          <CompanyInfo
            name={COMPANIES.FIVE_ELEMENT}
            websiteLink={'https://www.google.ru/'}
            address={'308033, Россия, г. Белгород, ул. Королёва, 2а, корп.2, оф. 536'}
            phone={'+7 (800) 555-30-53'}
            email={'rcm@reliab.tech'}
          >
            <div className={s.text}>
              <p>
                Компания «Пятый элемент» является разработчиком программных и технических систем и комплексов для
                бизнеса в сферах производства, ритейла, медицины, финансов, транспорта и логистики.
              </p>
              <br />
              <p>
                Разрабатывает приложения любой сложности с учетом потребностей и особенностей вашего бизнеса. Оказывает
                услуги по разработке, модернизации, тестированию и сопровождению ПО в форматах аутсорсинга,
                аутстаффинга, контрактной разработки и отдельных проектов. Специализируется на разработке программного
                обеспечения и решении задач по автоматизации бизнес-процессов,в том числе – уникальных и нестандартных.
              </p>
              <br />
              <p>
                Специалистами компании реализовано более 100 проектов – корпоративные системы, программные решения (в
                том числе - приложения для мобильных устройств), чат-боты, системы технического зрения.
              </p>
            </div>
          </CompanyInfo>
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export default FiveElement
