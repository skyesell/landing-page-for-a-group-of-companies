import s from '../styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { CompanyInfo } from '@ui/CompanyInfo'
import Contacts from '@ui/Contacts'
import COMPANIES from '@projectTypes/companies'

const ExpertChoice = () => {
  return (
    <MetaLayout pageTitle={COMPANIES.EXPERT_CHOICE_CIS}>
      <MainLayout>
        <div className={s.aboutCompany}>
          <CompanyInfo
            name={COMPANIES.EXPERT_CHOICE_CIS}
            websiteLink={'https://www.google.ru/'}
            address={'308033, Россия, Белгород, ул. Королёва, 2а, корпус 2'}
            phone={'+7 (800) 555-30-53'}
            email={'rcm@reliab.tech'}
          >
            <div className={s.text}>
              <p>Использование современных решений BPM и инструментов реализации программных продуктов и анализа.</p>
            </div>
          </CompanyInfo>
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export default ExpertChoice
