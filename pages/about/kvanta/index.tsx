import s from '../styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { CompanyInfo } from '@ui/CompanyInfo'
import Contacts from '@ui/Contacts'
import COMPANIES from '@projectTypes/companies'

const Kvanta = () => {
  return (
    <MetaLayout pageTitle={COMPANIES.KVANTA}>
      <MainLayout>
        <div className={s.aboutCompany}>
          <CompanyInfo
            name={COMPANIES.KVANTA}
            websiteLink={'https://www.google.ru/'}
            address={'308033, Россия, г. Белгород, ул. Королёва, д. 2А, корп. 2, оф. 617'}
            phone={'+7 (919) 432-93-65'}
            email={'info@kvanta.pro'}
          >
            <div className={s.text}>
              <p>
                Конструкторское бюро "Кванта" является российским разработчиком электроники. Оказывает услуги по
                контрактной разработке электронного оборудования, антенных систем и ПО на заказ. Специализируется в
                таких направлениях, как промышленная автоматика, IoT, робототехника, электроника для транспорта,
                робототехника, системы контроля доступа, GPS.
              </p>
              <br />
              <p>
                Специалисты компании обладают многолетним опытом контрактной разработки электроники, проектирования и
                разработки аппаратного и программного обеспечения для электронных устройств, оборудования и приборов
                любой сложности.
              </p>
            </div>
          </CompanyInfo>
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export default Kvanta
