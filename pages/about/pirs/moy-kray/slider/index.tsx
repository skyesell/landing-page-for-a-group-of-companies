import Image from 'next/image'
import React, { useRef, useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import s from './styles.module.scss'
import RightArrow from '@public/assets/project/pirs/moy-kray/rightArrow.svg'
import LeftArrow from '@public/assets/project/pirs/moy-kray/leftArrow.svg'
import cn from 'classnames'

const data = [
  {
    imgUrl: '/assets/project/pirs/moy-kray/horkina.png',
    imgUrlFull: '/assets/project/pirs/moy-kray/HorkinaFull.png',
    text: 'Светлана Хоркина',
  },
  {
    imgUrl: '/assets/project/pirs/moy-kray/SHuhov.png',
    imgUrlFull: '/assets/project/pirs/moy-kray/SHuhovFull.png',
    text: 'Шухов В.Г.',
  },
  {
    imgUrl: '/assets/project/pirs/moy-kray/Emelyanenko.png',
    imgUrlFull: '/assets/project/pirs/moy-kray/EmelyanenkoFull.png',
    text: 'Емельяненко Ф. В.',
  },
  {
    imgUrl: '/assets/project/pirs/moy-kray/Kosenkov.png',
    imgUrlFull: '/assets/project/pirs/moy-kray/KosenkovFull.png',
    text: 'Косенков С.С.',
  },
  {
    imgUrl: '/assets/project/pirs/moy-kray/Alekseev.png',
    imgUrlFull: '/assets/project/pirs/moy-kray/AlekseevFull.png',
    text: 'Алексеев И. А.',
  },
]

type sliderProps = {
  imgUrl: string
  imgUrlFull: string
  text: string
  onImgLoad: () => void
}

const Slide = ({ imgUrl, text, onImgLoad, imgUrlFull }: sliderProps) => {
  const [fullScreenImageOpened, setFullScreenImageOpened] = useState(false)
  const Modal = dynamic(() => import('@ui/Modal'), { ssr: false })

  const toggleFullScreenImage = () => {
    setFullScreenImageOpened((opened) => !opened)
  }

  return (
    <div className={s.slide}>
      <div className={s.imgDesktop}>
        <Image width={234} height={252} src={imgUrl} alt={'страница RCM'} onLoad={onImgLoad} />
      </div>
      <div className={s.imgMobile}>
        {/*<div onClick={toggleFullScreenImage} className={s.fullScreenlogo}>*/}
        {/*  <Image height={27} width={27} src="/assets/project/pirs/rcm/fullScreen.svg" />*/}
        {/*</div>*/}
        <Image
          onClick={toggleFullScreenImage}
          src={imgUrl}
          width={234}
          height={252}
          alt={'страница RCM'}
          onLoad={onImgLoad}
        />
        <Modal onClose={() => setFullScreenImageOpened(false)} opened={fullScreenImageOpened}>
          <img style={{ marginTop: '10px' }} src={imgUrlFull} alt={'img full'} />
        </Modal>
      </div>

      <div className={s.title}>
        <p>{text}</p>
      </div>
    </div>
  )
}

const Slider = () => {
  const [animating, setAnimating] = useState<boolean | null>(null)
  const [index, setIndex] = useState(0)
  const [isInitLoad, setInitLoad] = useState(true)

  const startAnimationTimeoutRef = useRef<NodeJS.Timeout | null>(null)
  const endAnimationTimeoutRef = useRef<NodeJS.Timeout | null>(null)

  useEffect(() => {
    return () => {
      if (startAnimationTimeoutRef.current) clearTimeout(startAnimationTimeoutRef.current)
      if (endAnimationTimeoutRef.current) clearTimeout(endAnimationTimeoutRef.current)
    }
  }, [])

  const handleAnimate = (callback: () => void) => {
    setAnimating(true)

    startAnimationTimeoutRef.current = setTimeout((): void => {
      callback()
    }, 300)
  }

  const disableAnimation = () => {
    if (isInitLoad) setInitLoad(false)
    else
      endAnimationTimeoutRef.current = setTimeout(() => {
        setAnimating(false)
      }, 300)
  }

  const handleNext = () => {
    if (!animating) {
      const newIndex = index + 1 >= data.length ? 0 : index + 1
      handleAnimate(() => setIndex(newIndex))
    }
  }

  const handlePrev = () => {
    if (!animating) {
      const newIndex = index - 1 < 0 ? data.length - 1 : index - 1
      handleAnimate(() => setIndex(newIndex))
    }
  }

  const handleStep = (newIndex: number) => {
    if (index !== newIndex && !animating) handleAnimate(() => setIndex(newIndex))
  }

  const slideWrapperClassName = cn({ [s.animationStart]: animating, [s.animationEnd]: animating === false })

  return (
    <div className={s.sliderMobile}>
      <div className={s.slideText}>
        <div className={s.arrow1} onClick={handlePrev}>
          <LeftArrow />
        </div>
        <div className={slideWrapperClassName}>
          <Slide
            text={data[index].text}
            imgUrl={data[index].imgUrl}
            imgUrlFull={data[index].imgUrlFull}
            onImgLoad={disableAnimation}
          />
        </div>
        <div className={s.arrow2} onClick={handleNext}>
          <RightArrow />
        </div>
      </div>

      <div className={s.stepper}>
        {data.map((_, stepIndex) => (
          <button
            key={stepIndex}
            className={cn(s.step, { [s.active]: stepIndex === index })}
            onClick={() => handleStep(stepIndex)}
          />
        ))}
      </div>
    </div>
  )
}

export default Slider
