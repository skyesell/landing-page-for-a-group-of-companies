import s from './styles.module.scss'
import Header from '@ui/Header'
import Image from 'next/image'
import Button from '@ui/Button'
import MenuButton from '@assets/project/pirs/rcm/menuButton.svg'
import { MetaLayout } from '@layouts/MetaLayout'
import { Breadcrumbs } from '@ui/Breadcrumbs'
import Footer from '@ui/Footer'
import Contacts from '@ui/Contacts'
import React, { useState } from 'react'
import dynamic from 'next/dynamic'
import styles from '@pageComponents/mainPage/Jumbotron/styles.module.scss'
import ContactForm from '@ui/ContactForm'
import COMPANIES from '@projectTypes/companies'
import Slider from './slider'

const MoyKray = () => {
  const [isModalOpened, setModalOpened] = useState(false)
  const Modal = dynamic(() => import('@ui/Modal'), { ssr: false })
  return (
    <>
      <Modal opened={isModalOpened} onClose={() => setModalOpened(false)}>
        <div className={styles.modalContent}>
          <h2>Мы свяжемся с Вами</h2>
          <p>Оставьте свои контактные данные и наш специалист ответит Вам в кратчайшие сроки</p>
          <ContactForm company={COMPANIES.DEFAULT} />
        </div>
      </Modal>
      <MetaLayout pageTitle={'Мой Край'}>
        <div className={s.mk}>
          <img className={s.logobackD} src={'/assets/project/pirs/moy-kray/logobackD.svg'} alt={'logo'} />
          <div className={s.headerBlock}>
            <Header theme={'light'} CustomMenuButton={MenuButton} />
            <img className={s.blueBean} src={'/assets/project/pirs/moy-kray/blueBean.svg'} alt={'blueBean'} />
          </div>
          <div className={s.headerLine} />
          <div className={s.mainSectionWrap}>
            <div className={s.breadcrumbsWrapper}>
              <Breadcrumbs
                currentPageName={'Мой Край'}
                modifyBreadcrumbStyle={s.breadcrumb}
                modifyLastBreadcrumbStyle={s.breadcrumbLast}
                modifyDividerStyle={s.breadcrumbDivider}
              />
            </div>
            <div className={s.mainSection}>
              <div className={s.blueBlock}>
                <img className={s.blue1} src={'/assets/project/pirs/moy-kray/blue1.svg'} alt={'blue'} />
              </div>
              <div className={s.info}>
                <div className={s.title}>
                  <img src={'/assets/project/pirs/moy-kray/logoMK.svg'} alt={'moy kray logo'} />
                </div>
                <p>
                  Социальная сеть для развития
                  <br />
                  внутреннего регионального туризма
                </p>

                <Button
                  text={'Связаться с нами'}
                  className={s.contactButton}
                  onClick={() => setModalOpened(true)}
                  type={'button'}
                />
              </div>
              <div className={s.humanBlock}>
                <a
                  className={s.appStore}
                  href={
                    'https://apps.apple.com/ru/app/%D0%BC%D0%BE%D0%B9-%D0%BA%D1%80%D0%B0%D0%B9-%D1%83%D0%B7%D0%BD%D0%B0%D0%B9-%D1%81%D0%B2%D0%BE%D0%B9-%D1%80%D0%B5%D0%B3%D0%B8%D0%BE%D0%BD/id1544924368?l=en'
                  }
                >
                  <Image src={'/assets/project/pirs/moy-kray/appStore.svg'} alt={'appStore'} width={113} height={29} />
                </a>
                <a
                  className={s.googlePlay}
                  href={'https://play.google.com/store/apps/details?id=ru.tech_reliab.myhomeland'}
                >
                  <Image
                    src={'/assets/project/pirs/moy-kray/googlePlay.svg'}
                    alt={'googlePlay'}
                    width={184}
                    height={38}
                  />
                </a>
                <img className={s.leaf5} src={'/assets/project/pirs/moy-kray/leaf1.svg'} alt={'leaf4'} />
                <img className={s.monument} src={'/assets/project/pirs/moy-kray/volodya.png'} alt={'Monument'} />
                <img className={s.manyBeans} src={'/assets/project/pirs/moy-kray/manyBeans.svg'} alt={'manyBeans'} />
                <img className={s.human} src={'/assets/project/pirs/moy-kray/human.svg'} alt={'appStore'} />
                <img className={s.humanD} src={'/assets/project/pirs/moy-kray/humanD.svg'} alt={'human'} />
                <img className={s.lev} src={'/assets/project/pirs/moy-kray/lev.png'} alt={'appStore'} />
                <img className={s.lightBean} src={'/assets/project/pirs/moy-kray/lightBean.svg'} alt={'lightBean'} />
                <img className={s.levD} src={'/assets/project/pirs/moy-kray/levD.png'} alt={'appStore'} />
                <img className={s.leaf4} src={'/assets/project/pirs/moy-kray/leaf4.svg'} alt={'leaf4'} />
              </div>
            </div>
            <div className={s.iphoneBlock}>
              <img
                className={s.iphone}
                width={305}
                height={614}
                src={'/assets/project/pirs/moy-kray/iPhone.png'}
                alt={'appStore'}
              />
            </div>
            <div className={s.infoBlocks}>
              <div>
                <div className={s.infoBlock1}>
                  <img
                    className={s.dottedLine1}
                    src={'/assets/project/pirs/moy-kray/dottedLine1.svg'}
                    alt={'dottedLine1'}
                  />
                  <h3>Брендинг территории</h3>
                  <p>
                    Формирование уникального бренда региона, развитие локальной культурной идентичности, а также
                    представление ее в ярких, взаимосвязанных образах, привлекательных для различных целевых аудиторий
                  </p>
                  <img className={s.leaf1} src={'/assets/project/pirs/moy-kray/leaf1.svg'} alt={'leaf1'} />
                </div>
                <div className={s.infoBlock2}>
                  <img
                    className={s.dottedLine2}
                    src={'/assets/project/pirs/moy-kray/dottedLine2.svg'}
                    alt={'dottedLine2'}
                  />
                  <h3>Культурная среда</h3>
                  <p>
                    Сохранение и преумножение культурного потенциала регионов за счет поощрения творческой активности,
                    развития культурных и исторических сообществ, организации фестивалей
                  </p>
                </div>
              </div>
              <div className={s.iphoneDBlock}>
                <img
                  className={s.iphoneD}
                  width={542}
                  height={784}
                  src={'/assets/project/pirs/moy-kray/iPhoneD.png'}
                  alt={'iPhoneD'}
                />
              </div>
              <div>
                <div className={s.infoBlock3}>
                  <img
                    className={s.dottedLine3}
                    src={'/assets/project/pirs/moy-kray/dottedLine3.svg'}
                    alt={'dottedLine3'}
                  />
                  <h3>Творческая индустрия</h3>
                  <img className={s.bee} src={'/assets/project/pirs/moy-kray/bee.svg'} alt={'bee'} />
                  <p>
                    Привлечение внимания к деятельности, в основе которой лежит творческая и ремесленная составляющая
                    (гончарное дело; ткачество; малые фермерские хозяйства; художественная ковка; литература;
                    потребительская кооперация; визуальные искусства и галерейный бизнес и другие)
                  </p>
                  <img className={s.leaf2} src={'/assets/project/pirs/moy-kray/leaf2.svg'} alt={'leaf2'} />
                </div>
                <div className={s.infoBlock4}>
                  <img
                    className={s.dottedLine4}
                    src={'/assets/project/pirs/moy-kray/dottedLine4.svg'}
                    alt={'dottedLine4'}
                  />
                  <h3>
                    Туристический
                    <br />
                    комплекс
                  </h3>
                  <p>
                    Привлечение внимания к малопосещаемым
                    <br />
                    территориям регионов, а также к вновь создаваемым объектам
                  </p>
                  <img className={s.leaf3} src={'/assets/project/pirs/moy-kray/leaf3.svg'} alt={'leaf3'} />
                </div>
              </div>
            </div>
            <div className={s.infoBlocksD}>
              <div className={s.infoBlocks1}>
                <div className={s.infoBlock1}>
                  <h3>Брендинг территории</h3>
                  <p>
                    Формирование уникального бренда региона, развитие локальной культурной идентичности, а также
                    представление ее в ярких, взаимосвязанных образах, привлекательных для различных целевых аудиторий
                  </p>
                  <img className={s.leaf1} src={'/assets/project/pirs/moy-kray/leaf4.svg'} alt={'leaf'} />
                </div>
                <div className={s.infoBlock3}>
                  <h3>Творческая индустрия</h3>
                  <p>
                    Привлечение внимания к деятельности, в основе которой лежит творческая и ремесленная составляющая
                    (гончарное дело; ткачество; малые фермерские хозяйства; художественная ковка; литература;
                    потребительская кооперация; визуальные искусства и галерейный бизнес и другие)
                  </p>
                  <img className={s.bee} src={'/assets/project/pirs/moy-kray/bee.svg'} alt={'bee'} />
                </div>
              </div>
              <div className={s.iphoneDBlock}>
                <img
                  className={s.iphoneD}
                  width={542}
                  height={784}
                  src={'/assets/project/pirs/moy-kray/iPhoneD.png'}
                  alt={'iPhoneD'}
                />
              </div>
              <div className={s.infoBlocks2}>
                <div className={s.infoBlock2}>
                  <img className={s.bee} src={'/assets/project/pirs/moy-kray/bee.svg'} alt={'bee'} />
                  <h3>Культурная среда</h3>
                  <p>
                    Сохранение и преумножение культурного потенциала регионов за счет поощрения творческой активности,
                    развития культурных и исторических сообществ, организации фестивалей
                  </p>
                </div>
                <div className={s.infoBlock4}>
                  <h3>Туристический комплекс</h3>
                  <p>
                    Привлечение внимания к малопосещаемым территориям регионов, а также к вновь создаваемым объектам
                  </p>
                  <img className={s.leaf3} src={'/assets/project/pirs/moy-kray/leaf3.svg'} alt={'leaf'} />
                </div>
              </div>
            </div>
            <div className={s.structure}>
              <h1>
                Структура
                <br />
                приложения
              </h1>
              <img
                className={s.dottedLineLong}
                src={'/assets/project/pirs/moy-kray/dottedLineLong.svg'}
                alt={'dottedLineLong'}
              />
              <img className={s.church} src={'/assets/project/pirs/moy-kray/church.svg'} alt={'church'} />

              <div style={{ position: 'relative', top: '280px' }}>
                <img className={s.groupLine1} src={'/assets/project/pirs/moy-kray/groupLine1.svg'} alt={'groupLine1'} />
                <h3>Каталог мест</h3>
                <p>
                  Перечень культурных, исторических, рекреационных, спортивных, религиозных и других социальных
                  объектов, а также памятников природы
                </p>
              </div>
              <div style={{ position: 'relative', top: '350px' }}>
                <h3>Лента событий</h3>
                <div style={{ display: 'flex' }}>
                  <div className={s.greenCircle} />
                  <p>Возможность делиться своими</p>
                </div>
                <p style={{ marginTop: '0', width: '265px' }}>
                  впечатлениями от туризма с другими пользователями (ближайших аналог – «Инстаграм»)
                </p>
                <div style={{ display: 'flex' }}>
                  <div className={s.greenCircle} />
                  <p>Культурные мероприятия, </p>
                </div>
                <p style={{ marginTop: '0' }}>фестивали, мастер-классы и другие активности региона</p>
                <img className={s.bee} src={'/assets/project/pirs/moy-kray/bee.svg'} alt={'bee'} />
                <img className={s.museum} src={'/assets/project/pirs/moy-kray/museum.svg'} alt={'museum'} />
              </div>
              <div style={{ position: 'relative', top: '520px' }}>
                <h3>Подборки</h3>
                <p>Тематические подборки мест и маршрутов, актуальных в конкретный период времени</p>
                <img className={s.humanMK} src={'/assets/project/pirs/moy-kray/humanMK.svg'} alt={'humanMK'} />
              </div>
              <div style={{ position: 'relative', top: '970px' }}>
                <h3>Аудиогиды</h3>
                <p>Возможность получать актуальную информацию об истории и особенностях посещаемых мест</p>
                <img className={s.bee2} src={'/assets/project/pirs/moy-kray/bee.svg'} alt={'bee'} />
              </div>
              <div style={{ position: 'relative', top: '1150px' }}>
                <img className={s.fountain} src={'/assets/project/pirs/moy-kray/fountain.svg'} alt={'fountain'} />
                <h3>Маршруты</h3>
                <div style={{ display: 'flex' }}>
                  <div className={s.greenCircle} />
                  <p>Возможность создавать</p>
                </div>
                <p style={{ marginTop: '0' }}>туристические маршруты и делиться ими с другими пользователями</p>
                <div style={{ display: 'flex' }}>
                  <div className={s.greenCircle} />
                  <p>Возможность проложить</p>
                </div>
                <p style={{ marginTop: '0' }}>навигацию к любому месту или к цепочке мест</p>
              </div>
              <div style={{ position: 'relative', top: '1200px' }}>
                <h3>Знаменитые земляки</h3>
                <img className={s.leaf1} src={'/assets/project/pirs/moy-kray/leaf1.svg'} alt={'leaf1'} />
                <p>
                  Великие исторические деятели, военные, ученые, спортсмены родного края представляют важную часть
                  культурного наследия региона. В приложении можно быстро найти и проложить маршрут к месту их рождения,
                  проживания, учебы и другой деятельности (и к связанным с ними монументам, памятным доскам и т.д.)
                </p>
                <img className={s.slideBack} src={'/assets/project/pirs/moy-kray/slideBack.svg'} alt={'sliderBack'} />
                <div style={{ position: 'absolute' }}>
                  <Slider />
                </div>
              </div>
            </div>
          </div>
          <div className={s.structureD}>
            <h1>Структура приложения</h1>
            <img
              className={s.lineStructureBack}
              src={'/assets/project/pirs/moy-kray/lineStructureBack.svg'}
              alt={'Back'}
            />
            <img className={s.church} src={'/assets/project/pirs/moy-kray/church.svg'} alt={'church'} />
            <div className={s.structure1}>
              <h3>Каталог мест</h3>
              <p>
                Перечень культурных, исторических, рекреационных, спортивных, религиозных и других социальных объектов,
                а также памятников природы
              </p>
            </div>
            <div className={s.structure2}>
              <div style={{ display: 'flex', marginRight: '10px' }}>
                <div>
                  <h3>Лента событий</h3>
                  <div style={{ display: 'flex', alignItems: 'baseline' }}>
                    <div className={s.greenCircle} />
                    <p>Возможность делиться своими</p>
                  </div>
                  <p style={{ minWidth: '265px', maxWidth: '320px' }}>
                    впечатлениями от туризма с другими пользователями (ближайших аналог – «Инстаграм»)
                  </p>

                  <div style={{ display: 'flex', alignItems: 'baseline' }}>
                    <div className={s.greenCircle} />
                    <p>Культурные мероприятия,</p>
                  </div>
                  <p>фестивали, мастер-классы и другие активности региона</p>
                </div>
              </div>
              <img className={s.museum} src={'/assets/project/pirs/moy-kray/museum.svg'} alt={'museum'} />
            </div>
            <div className={s.structureBlock}>
              <div style={{ position: 'relative', top: '-80px', left: '40px' }}>
                <h3>Аудиогиды</h3>
                <p>Возможность получать актуальную информацию об истории и особенностях посещаемых мест</p>
              </div>
              <img className={s.humanMK} src={'/assets/project/pirs/moy-kray/humanMKD.svg'} alt={'humanMK'} />
              <div style={{ position: 'relative', top: '-100px', left: '40px' }}>
                <h3>Подборки</h3>
                <p>Тематические подборки мест и маршрутов, актуальных в конкретный период времени</p>
              </div>
            </div>

            <div className={s.structureBlock2}>
              <div className={s.structure3}>
                <img className={s.fountain} src={'/assets/project/pirs/moy-kray/fountain.svg'} alt={'fountain'} />
                <h3>Маршруты</h3>
                <div style={{ display: 'flex', alignItems: 'baseline' }}>
                  <div className={s.greenCircle} />
                  <p>Возможность создавать туристические</p>
                </div>
                <p style={{ marginTop: '0', maxWidth: '368px' }}>маршруты и делиться ими с другими пользователями</p>
                <div style={{ display: 'flex', alignItems: 'baseline' }}>
                  <div className={s.greenCircle} />
                  <p>Возможность проложить навигацию к</p>
                </div>
                <p style={{ marginTop: '0', maxWidth: '368px' }}> любому месту или к цепочке мест</p>
              </div>
              <div className={s.structure4}>
                <div className={s.text}>
                  <h3>Знаменитые земляки</h3>
                  <p>
                    Великие исторические деятели, военные, ученые, спортсмены родного края представляют важную часть
                    культурного наследия региона.
                    <br />В приложении можно быстро найти и проложить маршрут к месту их рождения, проживания, учебы и
                    другой деятельности (и к связанным с ними монументам, памятным доскам и т.д.)
                  </p>
                </div>
                <div className={s.sliderBlock}>
                  <div className={s.slider}>
                    <Slider />
                  </div>
                  <img className={s.slideBack} src={'/assets/project/pirs/moy-kray/slideBack.svg'} alt={'sliderBack'} />
                </div>
              </div>
            </div>
          </div>
          <div className={s.backgroundContacts}>
            <img
              className={s.backgroundImg}
              src={'/assets/project/pirs/moy-kray/logoBackMob.svg'}
              alt={'backgroundImg'}
            />
            <Contacts
              disableHighlight={true}
              mapsImgUrl={'/assets/lightMap1.svg'}
              gisImgUrl={'/assets/lightMap2.svg'}
              buttonColor={'#4460F1'}
              inputColor={'#FFFFFF'}
              headerColor={'#FFFFFF'}
              textColor={'#FFFFFF'}
            />
          </div>
        </div>
        <div style={{ marginTop: '-100px' }}>
          <Footer />
        </div>
      </MetaLayout>
    </>
  )
}

export default MoyKray
