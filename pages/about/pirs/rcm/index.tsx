import s from './styles.module.scss'
import Header from '@ui/Header'
import Image from 'next/image'
import Button from '@ui/Button'
import previewMob from '@assets/project/pirs/rcm/previewMob.webp'
import MenuButton from '@assets/project/pirs/rcm/menuButton.svg'
import { MetaLayout } from '@layouts/MetaLayout'
import { Breadcrumbs } from '@ui/Breadcrumbs'
import Footer from '@ui/Footer'
import Slider from './slider'
import Contacts from '@ui/Contacts'
import React, { useState } from 'react'
import dynamic from 'next/dynamic'
import styles from '@pageComponents/mainPage/Jumbotron/styles.module.scss'
import ContactForm from '@ui/ContactForm'
import COMPANIES from '@projectTypes/companies'

const Rcm = () => {
  const [isModalOpened, setModalOpened] = useState(false)
  const Modal = dynamic(() => import('@ui/Modal'), { ssr: false })
  return (
    <>
      <Modal opened={isModalOpened} onClose={() => setModalOpened(false)}>
        <div className={styles.modalContent}>
          <h2>Мы свяжемся с Вами</h2>
          <p>Оставьте свои контактные данные и наш специалист ответит Вам в кратчайшие сроки</p>
          <ContactForm company={COMPANIES.DEFAULT} />
        </div>
      </Modal>
      <MetaLayout pageTitle={'RCM'}>
        <div className={s.rcm}>
          <div
            style={{
              backgroundImage: `url('/assets/project/pirs/rcm/background.webp')`,
              background: 'linear-gradient(77.64deg, #4E4BE7 1.77%, #1F5EC4 64.16%)',
              backgroundColor: '#0062FF',
            }}
          >
            <Header theme={'light'} CustomMenuButton={MenuButton} />
            <div className={s.mainSectionWrap}>
              <div className={s.breadcrumbsWrapper}>
                <Breadcrumbs
                  currentPageName={'RCM Навигатор'}
                  modifyBreadcrumbStyle={s.breadcrumb}
                  modifyLastBreadcrumbStyle={s.breadcrumbLast}
                  modifyDividerStyle={s.breadcrumbDivider}
                />
              </div>
              <div className={s.mainSection}>
                <div className={s.info}>
                  <p className={s.name}>RCM Навигатор</p>
                  <p>
                    Cистема контроля <br /> состояния рабочего оборудования и промышленной автоматизации
                  </p>
                  <Button
                    text={'Связаться с нами'}
                    className={s.contactButton}
                    onClick={() => setModalOpened(true)}
                    type={'button'}
                  />
                </div>
                <div className={s.previewMobile}>
                  <Image src={previewMob} alt={'Project preview'} layout={'intrinsic'} />
                </div>
                <div className={s.preview}>
                  <Image
                    src={'/assets/project/pirs/rcm/preview.webp'}
                    height={428}
                    width={957}
                    alt={'Project preview'}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className={s.customerSection}>
            <div className={s.aboutCustomer}>
              <h2>Что такое RCM?</h2>
              <p>
                <b> RCM </b> (Reliability Centered Maintenance — англ.) — это методология, позволяющая сформировать
                стратегию технического обслуживания,
                <br />
                направленную на обеспечение надёжности оборудования.
              </p>
              <p>Другими словами, это комплексная методика, целями которой являются:</p>
              <div className={s.infoBlock}>
                <Image src={'/assets/project/pirs/rcm/blocks.png'} width={1338} height={178} alt={'info blocks'} />
              </div>
              <div className={s.infoBlocks}>
                <Image src={'/assets/project/pirs/rcm/objectRCM1.png'} width={288} height={158} alt={'info blocks'} />
                <Image src={'/assets/project/pirs/rcm/objectRCM2.png'} width={288} height={158} alt={'info blocks'} />
                <Image src={'/assets/project/pirs/rcm/objectRCM3.png'} width={288} height={158} alt={'info blocks'} />
              </div>
            </div>
          </div>
          <Slider />
          <div className={s.details}>
            <h2>Детали</h2>
            <p style={{ maxWidth: '1338px' }}>
              Одной из ключевых особенностей Pack RCM Навигатор является возможность его
              <b> интеграции с системами ТОиР </b>
            </p>
            <p style={{ maxWidth: '1338px' }}>
              (автоматизированная система контроля технического обслуживания и ремонта оборудования) для передачи
              информации&nbsp;об&nbsp;инцидентах, являющихся основанием для формирования наряд-заказа на выполнение
              ремонтных работ.
            </p>
            <div className={s.gearsBlock}>
              <p>
                <b>Активы предприятия</b>
                <br />
                Производственное оборудование
              </p>
              <Image src={'/assets/project/pirs/rcm/gears.png'} width={319} height={250} alt={'Gears'} />
              <p>
                <b>RCM Навигатор</b>
                <br />
                Риск-ориентированный
                <br />
                подход к управлению активами
              </p>
              <p>
                <b>ТОиР</b>
                <br />
                Управление ремонтами
                <br />и обслуживанием оборудования
              </p>
            </div>
            <div className={s.gearsBlock2}>
              <div className={s.gearInfo}>
                <Image src={'/assets/project/pirs/rcm/gear.svg'} width={49} height={42} alt={'Gears'} />
                <p>
                  <b>Активы предприятия</b> Производственное оборудование
                </p>
              </div>
              <div className={s.gearInfo}>
                <Image src={'/assets/project/pirs/rcm/gearActive.svg'} width={49} height={42} alt={'Gears'} />
                <p>
                  <b>RCM Навигатор</b>
                  <br />
                  Риск-ориентированный подход к управлению активами
                </p>
              </div>
              <div className={s.gearInfo}>
                <Image src={'/assets/project/pirs/rcm/gear.svg'} width={49} height={42} alt={'Gears'} />
                <p>
                  <b>ТОиР </b>
                  <br />
                  Управление ремонтами и обслуживанием оборудования
                </p>
              </div>
            </div>
          </div>
          <div className={s.mondiInfo}>
            <div className={s.mondiLogo}>
              <Image src={'/assets/project/pirs/rcm/mondi.svg'} width={275} height={113} alt={'Mondi logo'} />
            </div>
            <p>
              В настоящий момент данная система внедрена в АО "МОНДИ СЛПК" (Mondi Group) и проходит пилотирование еще на
              ряде предприятий.
            </p>
          </div>

          <Contacts
            disableHighlight={true}
            mapsImgUrl={'/assets/lightMap1.svg'}
            gisImgUrl={'/assets/lightMap2.svg'}
            buttonColor={'#4460F1'}
            inputColor={'#FFFFFF'}
          />
          <Footer />
        </div>
      </MetaLayout>
    </>
  )
}

export default Rcm
