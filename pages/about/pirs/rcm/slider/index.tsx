import Image from 'next/image'
import React, { useRef, useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import s from './styles.module.scss'
import RightArrow from '@public/assets/project/pirs/rcm/rightArrow.svg'
import LeftArrow from '@public/assets/project/pirs/rcm/leftArrow.svg'
import cn from 'classnames'

const data = [
  {
    imgUrl: '/assets/project/pirs/rcm/rcm-1.png',
    text: 'Веб-интерфейс для управления классификатором активов предприятия',
  },
  { imgUrl: '/assets/project/pirs/rcm/rcm-2.png', text: 'Раздел для выполнения RCM-анализа' },
  {
    imgUrl: '/assets/project/pirs/rcm/rcm-3.png',
    text: 'Интерфейс для управления индикаторами состояния оборудования',
  },
  { imgUrl: '/assets/project/pirs/rcm/rcm-4.png', text: 'Блок планирования и отслеживания хода осмотров оборудования' },
  {
    imgUrl: '/assets/project/pirs/rcm/rcm-5.png',
    text: 'Мобильное приложение для специалистов, осуществляющих обход оборудования и внесение данных по его состоянию',
  },
]

type sliderProps = {
  imgUrl: string
  text: string
  onImgLoad: () => void
}

const Slide = ({ imgUrl, text, onImgLoad }: sliderProps) => {
  const [fullScreenImageOpened, setFullScreenImageOpened] = useState(false)
  const Modal = dynamic(() => import('@ui/Modal'), { ssr: false })

  const toggleFullScreenImage = () => {
    setFullScreenImageOpened((opened) => !opened)
  }

  return (
    <div className={s.slide}>
      <div className={s.title}>
        <p>{text}</p>
      </div>
      <div className={s.imgDesktop}>
        <Image src={imgUrl} width={849} height={447} alt={'страница RCM'} onLoad={onImgLoad} />
      </div>

      <div className={s.imgMobile}>
        <div onClick={toggleFullScreenImage} className={s.fullScreenlogo}>
          <Image height={27} width={27} src="/assets/project/pirs/rcm/fullScreen.svg" />
        </div>
        <Image
          onClick={toggleFullScreenImage}
          src={imgUrl}
          width={265}
          height={133}
          alt={'страница RCM'}
          onLoad={onImgLoad}
        />
        <Modal onClose={() => setFullScreenImageOpened(false)} opened={fullScreenImageOpened}>
          <img style={{ marginTop: '40px' }} src={imgUrl} />
        </Modal>
      </div>
    </div>
  )
}

const Slider = () => {
  const [animating, setAnimating] = useState<boolean | null>(null)
  const [index, setIndex] = useState(0)
  const [isInitLoad, setInitLoad] = useState(true)

  const startAnimationTimeoutRef = useRef<NodeJS.Timeout | null>(null)
  const endAnimationTimeoutRef = useRef<NodeJS.Timeout | null>(null)

  useEffect(() => {
    return () => {
      if (startAnimationTimeoutRef.current) clearTimeout(startAnimationTimeoutRef.current)
      if (endAnimationTimeoutRef.current) clearTimeout(endAnimationTimeoutRef.current)
    }
  }, [])

  const handleAnimate = (callback: () => void) => {
    setAnimating(true)

    startAnimationTimeoutRef.current = setTimeout((): void => {
      callback()
    }, 300)
  }

  const disableAnimation = () => {
    if (isInitLoad) setInitLoad(false)
    else
      endAnimationTimeoutRef.current = setTimeout(() => {
        setAnimating(false)
      }, 300)
  }

  const handleNext = () => {
    if (!animating) {
      const newIndex = index + 1 >= data.length ? 0 : index + 1
      handleAnimate(() => setIndex(newIndex))
    }
  }

  const handlePrev = () => {
    if (!animating) {
      const newIndex = index - 1 < 0 ? data.length - 1 : index - 1
      handleAnimate(() => setIndex(newIndex))
    }
  }

  const handleStep = (newIndex: number) => {
    if (index !== newIndex && !animating) handleAnimate(() => setIndex(newIndex))
  }

  const slideWrapperClassName = cn({ [s.animationStart]: animating, [s.animationEnd]: animating === false })

  return (
    <>
      <div className={s.sliderDesktop}>
        <div className={s.arrow1} onClick={handlePrev}>
          <LeftArrow />
        </div>
        <div>
          <h2>
            Pack RCM Навигатор <br /> включает в себя:
          </h2>
          <div className={s.stepper}>
            {data.map((_, stepIndex) => (
              <button
                key={stepIndex}
                className={cn(s.step, { [s.active]: stepIndex === index })}
                onClick={() => handleStep(stepIndex)}
              />
            ))}
          </div>
          <div className={slideWrapperClassName}>
            <Slide text={data[index].text} imgUrl={data[index].imgUrl} onImgLoad={disableAnimation} />
          </div>
        </div>
        <div className={s.arrow2} onClick={handleNext}>
          <RightArrow />
        </div>
      </div>

      <div className={s.sliderMobile}>
        <div className={s.slideText}>
          <h2>
            Pack RCM Навигатор <br /> включает в себя:
          </h2>
          <div className={slideWrapperClassName}>
            <Slide text={data[index].text} imgUrl={data[index].imgUrl} onImgLoad={disableAnimation} />
          </div>
        </div>
        <div className={s.navigation}>
          <div className={s.arrow1} onClick={handlePrev}>
            <LeftArrow />
          </div>
          <div className={s.stepper}>
            {data.map((_, stepIndex) => (
              <button
                key={stepIndex}
                className={cn(s.step, { [s.active]: stepIndex === index })}
                onClick={() => handleStep(stepIndex)}
              />
            ))}
          </div>
          <div className={s.arrow2} onClick={handleNext}>
            <RightArrow />
          </div>
        </div>
      </div>
    </>
  )
}

export default Slider
