import s from '../styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { CompanyInfo } from '@ui/CompanyInfo'
import Image from 'next/image'
import Contacts from '@ui/Contacts'
import Link from 'next/link'
import COMPANIES from '@projectTypes/companies'

const Pirs = () => {
  return (
    <MetaLayout pageTitle={COMPANIES.PIRS}>
      <MainLayout>
        <div className={s.aboutCompany}>
          <CompanyInfo
            name={COMPANIES.PIRS}
            websiteLink={'https://www.google.ru/'}
            address={'308034, Россия, г. Белгород, ул. Королёва, д. 2а, корпус 2, офис 512'}
            phone={'+7 (800) 555-30-53'}
            email={'tn@reliab.tech'}
          >
            <div className={s.text}>
              <p>
                Компания ПИРС является разработчиком программных и технических систем для бизнеса в сферах производства,
                медицины, ритейла, финансов и транспорта.
              </p>
              <br />
              <p>
                Компания оказывает услуги по созданию, доработке, тестированию и сопровождению ПО в формате отдельных
                проектов и в формате аутсорсинга.
              </p>
              <br />
              <p>
                Так же компания развивает собственное направление R&D, специализирующееся на системах технического
                зрения, машинном обучении и задачах классификации данных.
              </p>
            </div>
          </CompanyInfo>
          <div className={s.collageMob}>
            <Link href={'/about/pirs/rcm'}>
              <a>
                <Image src={'/assets/project/pirs/rcm.png'} alt={'rcm'} height={271} width={271} />
              </a>
            </Link>
            <Image src={'/assets/project/pirs/kubu.png'} alt={'kugbu'} height={853} width={562} />
            <Link href={'/about/pirs/moy-kray'}>
              <a>
                <Image src={'/assets/project/pirs/moykray.png'} alt={'Moy Kray'} height={271} width={562} />
              </a>
            </Link>
            <Image src={'/assets/project/pirs/smartMarket.png'} alt={'Smart Market'} height={271} width={562} />
            <Image src={'/assets/project/pirs/bot.png'} alt={'Bot'} height={271} width={562} />
          </div>

          <div className={s.collage}>
            <div>
              <Link href={'/about/pirs/rcm'}>
                <a>
                  <Image src={'/assets/project/pirs/rcm.png'} alt={'rcm'} height={562} width={562} />
                </a>
              </Link>
              <Image src={'/assets/project/pirs/emergency.png'} alt={'emergency'} height={271} width={562} />
            </div>
            <div>
              <Image src={'/assets/project/pirs/kubu.png'} alt={'kugbu'} height={853} width={562} />
            </div>
            <div>
              <Link href={'/about/pirs/moy-kray'}>
                <a>
                  <Image src={'/assets/project/pirs/moykray.png'} alt={'Moy Kray'} height={271} width={562} />
                </a>
              </Link>
              <Image src={'/assets/project/pirs/smartMarket.png'} alt={'Smart Market'} height={271} width={562} />
              <Image src={'/assets/project/pirs/bot.png'} alt={'Bot'} height={271} width={562} />
            </div>
          </div>
        </div>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export default Pirs
