import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { MainInfo } from '@pageComponents/about/MainInfo'
import Contacts from '@ui/Contacts'
import s from './styles.module.scss'
import GroupCompany from '@ui/GroupCompany'

const About = () => {
  return (
    <MetaLayout pageTitle="О нас">
      <MainLayout>
        <div className={s.about}>
          <MainInfo />
          <GroupCompany />
          <Contacts />
        </div>
      </MainLayout>
    </MetaLayout>
  )
}

export default About
