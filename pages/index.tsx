import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import Jumbotron from '@pageComponents/mainPage/Jumbotron'
import GroupCompany from '../ui/GroupCompany'
import Jobs from '../pageComponents/mainPage/Jobs'
import Contacts from '../ui/Contacts'

const Home = () => {
  return (
    <>
      <MetaLayout pageTitle="Технологии надежности">
        <MainLayout>
          <Jumbotron />
          <GroupCompany />
          <Jobs />
          <Contacts />
        </MainLayout>
      </MetaLayout>
    </>
  )
}

export default Home
