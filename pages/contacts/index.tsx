import dynamic from 'next/dynamic'

import s from './styles.module.scss'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { ContactItem } from '@ui/ContactItem'
import { Breadcrumbs } from '@ui/Breadcrumbs'
import ContactsForm from '@ui/Contacts'
import { BackgroundHighlight } from '@ui/BackgroundHighlight'

const Contacts = () => {
  const Map = dynamic(() => import('@ui/Map'), { ssr: false })

  return (
    <MetaLayout pageTitle={'Контакты'}>
      <MainLayout>
        <div className={s.contacts}>
          <BackgroundHighlight color={'#E754FF'} size={175} className={s.hl1} />
          <BackgroundHighlight color={'#00FFFF'} size={175} className={s.hl2} />
          <BackgroundHighlight color={'#9753DD'} size={170} className={s.hl3} />
          <Breadcrumbs />
          <h1>Контакты</h1>
          <div className={s.items}>
            <ContactItem
              label={'Адрес'}
              iconPath={'/assets/addressIcon.svg'}
              data={'308034, Россия, г. Белгород,\n' + 'ул. Королёва, д. 2а, корпус 2, офис 512'}
              kind={'text'}
            />
            <ContactItem label={'Приёмная'} iconPath={'/assets/tel.svg'} data={'+7 (800) 555-30-53'} kind={'phone'} />
            <ContactItem label={'Менеджер'} iconPath={'/assets/email.svg'} data={'tn@reliab.tech'} kind={'email'} />
          </div>
        </div>
        <div className={s.map}>
          <Map />
        </div>
        <ContactsForm />
      </MainLayout>
    </MetaLayout>
  )
}

export default Contacts
