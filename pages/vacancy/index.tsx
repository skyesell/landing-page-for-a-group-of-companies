import { useEffect, useRef, useState } from 'react'
import cs from 'classnames'

import s from './styles.module.scss'
import { VacancyT, vacancyApi } from '../../api/vacancy'
import { MetaLayout } from '@layouts/MetaLayout'
import { MainLayout } from '@layouts/MainLayout'
import { VacancyItem } from '@pageComponents/vacancy/VacancyItem'
import { Breadcrumbs } from '@ui/Breadcrumbs'
import Contacts from '@ui/Contacts'
import { useRequestAnimation } from '@utils/useRequestAnimation'

const Vacancy = ({ vacancies }: { vacancies: VacancyT[] }) => {
  const [currentVacancyId, setCurrentVacancyId] = useState<number | null>(null)
  const [maxItems, setMaxItems] = useState(10)
  const [itemsAnimate, setItemsAnimate] = useState(false)

  const itemsRef = useRef<HTMLDivElement>(null)
  const timeoutRef = useRef<any>(0)
  const itemsArrayRef = useRef<Array<HTMLDivElement | null>>([])
  const currentVacancyIdRef = useRef(currentVacancyId)

  useEffect(() => {
    return () => {
      clearTimeout(timeoutRef.current)
    }
  }, [])

  useEffect(() => {
    currentVacancyIdRef.current = currentVacancyId
  }, [currentVacancyId])

  useRequestAnimation(() => {
    if (itemsRef.current) {
      itemsRef.current.style.height = `${itemsArrayRef.current.reduce(
        (acc, item) => (item ? acc + item.getBoundingClientRect().height : acc),
        0
      )}px`
    }
  })

  const handleShowMore = () => {
    setMaxItems(maxItems + 10)
    setItemsAnimate(true)
    timeoutRef.current = setTimeout(() => {
      setItemsAnimate(false)
    }, 200)
  }

  return (
    <MetaLayout pageTitle={'Вакансии'}>
      <MainLayout>
        <main className={s.vacancy}>
          <Breadcrumbs />
          <h1>Вакансии</h1>
          <div className={cs(s.items, { [s.animate]: itemsAnimate })} ref={itemsRef}>
            {vacancies.map(
              (vacancy, index) =>
                index < maxItems && (
                  <VacancyItem
                    key={vacancy.id}
                    ref={(el) => (itemsArrayRef.current[index] = el)}
                    vacancyName={vacancy.vacancyName}
                    publicationDate={vacancy.publicationDate}
                    companyName={vacancy.companyName}
                    vacancyDescription={vacancy.vacancyDescription}
                    companyEmail={vacancy.companyEmail}
                    isOpen={vacancy.id === currentVacancyId}
                    onOpen={() => setCurrentVacancyId(vacancy.id === currentVacancyId ? null : vacancy.id)}
                  />
                )
            )}
          </div>
          <button className={cs(s.showMore, { [s.hidden]: maxItems > vacancies.length })} onClick={handleShowMore}>
            Показать еще
          </button>
        </main>
        <Contacts />
      </MainLayout>
    </MetaLayout>
  )
}

export const getServerSideProps = async () => {
  const vacancies = await vacancyApi.getVacancies()
  return { props: { vacancies } }
}

export default Vacancy
