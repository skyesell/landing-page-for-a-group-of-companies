import { http } from '@api'
import markdownToHtml from '@utils/markdownToHtml'

export type VacancyT = {
  id: number
  vacancyName: string
  publicationDate: string
  companyName: string
  vacancyDescription: string
  companyEmail: string
  telegramLink?: string
}

export type VacancyDTO = Omit<VacancyT, 'id' | 'publicationDate'>

export type VacancyRes = {
  data: {
    id: number
    attributes: VacancyDTO & { publicationDate: Date }
  }[]
  meta: {
    pagination: { page: number; pageSize: number; pageCount: number; total: number }
  }
}

export const vacancyApi = {
  getVacancies: async (): Promise<VacancyT[]> => {
    const res = await http.get<VacancyRes>('/vacancies')

    return Promise.all(
      res.data.data.map(async (vacancy) => ({
        ...vacancy.attributes,
        id: vacancy.id,
        publicationDate: new Date(vacancy.attributes.publicationDate).toLocaleDateString('RU-ru'),
        vacancyDescription: await markdownToHtml(vacancy.attributes.vacancyDescription),
      }))
    )
  },
}
