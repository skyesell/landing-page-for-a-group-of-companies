type SendEmail = {
  to: string
  from: string
  text: string
  subject?: string
}

export type { SendEmail }
