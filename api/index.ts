import axios from 'axios'
import emailApi from './email'

export type PaginationT = { page: number; pageSize: number; pageCount: number; total: number }

export const baseUrl = process.env.HOST ?? ''
export const baseUrlImg = ''

export const http = axios.create({
  baseURL: `${baseUrl ?? ''}/api`,
})

export { emailApi }
