import { baseUrlImg, http } from '@api'
import markdownToHtml from '@utils/markdownToHtml'
import { BlogT, MultipleBlogsDoneData, MultipleBlogsDTO, SingleBlogDTO } from '@api/news'

export const newsApi = {
  getMultipleBlogs: async (page?: number): Promise<MultipleBlogsDoneData> => {
    const res = await http.get<MultipleBlogsDTO>('/blogs', {
      params: { populate: '*', 'pagination[page]': page ?? 1, 'pagination[pageSize]': 5 },
    })
    const attributes = res.data.data.map((blog) => ({
      ...blog.attributes,
      id: blog.id,
      publicationDate: new Date(blog.attributes.publicationDate).toLocaleDateString('RU-ru'),
    }))

    return { attributes: attributes, pagination: res.data.meta.pagination }
  },
  getSingleBlog: async (friendlyURL: string): Promise<BlogT> => {
    const res = await http.get<SingleBlogDTO>(`/blogs`, { params: { 'filters[friendlyURL][$eq]': friendlyURL } })
    const data = res.data.data[0]
    //Регулярка чтобы урл менять, потому что страпи не подменяет значение из бд
    const baseUrlRegex = /https?:\/\/[^#?/]+/g
    return {
      ...data.attributes,
      id: data.id,
      description: await markdownToHtml(data.attributes.description.replace(baseUrlRegex, baseUrlImg as string)),
      publicationDate: new Date(data.attributes.publicationDate).toLocaleDateString('RU-ru'),
    }
  },
}

export * from './types'
