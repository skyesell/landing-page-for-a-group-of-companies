import { PaginationT } from '@api'

export type BlogT = {
  id: number
  title: string
  previewDescription?: string
  description: string
  publicationDate: string
  friendlyURL: string
  thumbnail?: {
    data: {
      attributes: {
        url: string
        height: number
        width: number
      }
    }
  }
}

export type MultipleBlogsDTO = {
  data: {
    id: number
    attributes: Omit<BlogT, 'id' | 'publicationDate'> & { publicationDate: Date }
  }[]
  meta: {
    pagination: { page: number; pageSize: number; pageCount: number; total: number }
  }
}

export type MultipleBlogsDoneData = {
  attributes: BlogT[]
  pagination: PaginationT
}

export type SingleBlogDTO = {
  data: Array<{ id: number; attributes: Omit<BlogT, 'id' | 'publicationDate'> & { publicationDate: Date } }>
}
