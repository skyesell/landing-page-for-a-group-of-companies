enum COMPANIES {
  PIRS = 'Пирс',
  GEOEXPERT = 'Geoexpert',
  KVANTA = 'KVANTA',
  FIVE_ELEMENT = 'Пятый элемент',
  EXPERT_CHOICE_CIS = 'Expert Choice CIS',
  SKYPOWER = 'Skypower',
  DEFAULT = 'default',
  KATESPARK = 'Kate Spark',
}

export default COMPANIES
